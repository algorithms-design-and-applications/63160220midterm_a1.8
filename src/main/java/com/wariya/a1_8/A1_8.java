/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.wariya.a1_8;

import java.util.Scanner;


/**
 *
 * @author wariy
 */
public class A1_8 {

      public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter element array: ");
        String arrString = input.nextLine();
        String[] arr = arrString.split(",");
        String[] reverseArr = reverse(arr, arr.length);

        System.out.println("Reversed array is: \n");
        for (int k = 0; k < reverseArr.length; k++) {
            if(k != reverseArr.length - 1){
                System.out.print(reverseArr[k] + ",");
            } else {
                System.out.print(reverseArr[k]);
            }
            
        }
    }

    public static String[] reverse(String a[], int n)
    {
        String[] reverseArr = new String[n];
        int j = n;
        for (int i = 0; i < n; i++) {
            reverseArr[j - 1] = a[i];
            j = j - 1;
        }
        return reverseArr;
    }
}
